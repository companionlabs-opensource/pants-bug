pytest
# Use httpx instead of requests as it support both synchronous and async HTTP with more or less the same
# API as requests.
httpx
pyyaml

# Type hints
types-PyYAML
