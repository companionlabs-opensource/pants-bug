from pathlib import Path
from typing import Dict

import yaml


def example(src: Path) -> Dict[str, str]:
    with open(src, 'r') as inf:
        return yaml.safe_load(inf)
