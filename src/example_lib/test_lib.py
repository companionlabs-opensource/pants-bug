from pathlib import Path
from tempfile import TemporaryDirectory

from .lib import example


def test_example():
    with TemporaryDirectory() as td:
        tdp = Path(td)
        ymlp = tdp / 'example.yml'
        with open(ymlp, 'w') as outf:
            outf.write('hello: world\n')
        res = example(ymlp)
        assert res == {'hello': 'world'}
